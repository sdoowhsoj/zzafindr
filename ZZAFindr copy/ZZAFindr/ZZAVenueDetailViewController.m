//
//  ZZAVenueDetailViewController.m
//  ZZAFindr
//
//  Created by Josh Woods on 4/20/14.
//  Copyright (c) 2014 sdoowhsoj. All rights reserved.
//

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#import "ZZAVenueDetailViewController.h"

@interface ZZAVenueDetailViewController ()

@end

@implementation ZZAVenueDetailViewController
{
    MKMapView *mapView;
    NSString *phoneNumber;
    double coordinates;
}

- (IBAction)phoneCall:(id)sender
{
    NSString *phoneString = [NSString stringWithFormat:@"tel://%@", phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneString]];
}

- (IBAction)website:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.venue.url]];
}

- (IBAction)review:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.venue.reviewUrl]];
}

/*- (IBAction)openmaps:(id)sender
 {
 NSString *ll = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@ %@,%@",self.venue.location.address, self.venue.location.city, self.venue.location.state];
 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ll]];
 }*/

+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress
{
    NSData* data = [NSData dataWithContentsOfURL: [NSURL URLWithString: urlAddress] ];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

-(NSData*)toJSON
{
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

-(void)viewDidLayoutSubviews
{
    [self.venueDetailsScrollView setContentSize:CGSizeMake(320, 500)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0.945 green:0.851 blue:0.6 alpha:1];

    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.749 green:0.224 blue:0.173 alpha:1];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor colorWithRed:0.945 green:0.851 blue:0.6 alpha:1], NSForegroundColorAttributeName,
                                                                     [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:24.0], NSFontAttributeName, nil]];
    self.navigationItem.title = self.venue.name;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.detailButton.titleLabel.lineBreakMode = 0;
    self.reviewLabel.titleLabel.lineBreakMode = 0;
    
    //review excerpt logic
    if(self.venue.excerpt != nil)
    {
        [self.reviewLabel setTitle:[NSString stringWithFormat:@"\"%@read more\"", self.venue.excerpt] forState:UIControlStateNormal];
    } else {
        self.reviewLabel.hidden = YES;
    }
    
    //address formatting button logic
    if(self.venue.address3 != nil){
    [self.detailButton setTitle:[NSString stringWithFormat:@"%@\n%@\n%@\n%@, %@", self.venue.address1,self.venue.address2,self.venue.address3, self.venue.city, self.venue.state] forState:UIControlStateNormal];
    } else if(self.venue.address2 != nil){
        [self.detailButton setTitle:[NSString stringWithFormat:@"%@\n%@\n%@, %@", self.venue.address1,self.venue.address2, self.venue.city, self.venue.state] forState:UIControlStateNormal];
    } else {
        [self.detailButton setTitle:[NSString stringWithFormat:@"%@\n%@, %@", self.venue.address1, self.venue.city, self.venue.state] forState:UIControlStateNormal];
    }
    
    //phone number enabled logic
    if(self.venue.phoneNumber != nil)
    {
        self.phoneCallButton.enabled = YES;
        self.phoneLabel.text = self.venue.phoneNumber;
        phoneNumber = self.venue.phoneNumber;
    } else {
        self.phoneCallButton.enabled = NO;
        self.phoneLabel.text = @"No Phone Number Listed";
    }
    
    //yelp url logic
    if((self.venue.url == nil))
    {
        self.urlButton.hidden = YES;
    }
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D myCoordinate;
    myCoordinate.latitude = [self.venue.venueLatitude doubleValue];
    myCoordinate.longitude = [self.venue.venueLongitude doubleValue];
    annotation.coordinate = myCoordinate;
    [mapView addAnnotation:annotation];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (myCoordinate, 800, 800);
    [mapView setRegion:region];
}

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *annotationViewReuseIdentifier = @"annotationViewReuseIdentifier";
    
    MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationViewReuseIdentifier];
    
    if (annotationView == nil)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationViewReuseIdentifier];
    }
    
    annotationView.image = [UIImage imageNamed:@"pizzapin"];
    annotationView.annotation = annotation;
    
    return annotationView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
