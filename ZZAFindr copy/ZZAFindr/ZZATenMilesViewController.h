//
//  ZZATenMilesViewController.h
//  ZZAFindr
//
//  Created by Josh Woods on 4/24/14.
//  Copyright (c) 2014 sdoowhsoj. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreLocation;
@import QuartzCore;

@interface ZZATenMilesViewController : UIViewController <CLLocationManagerDelegate>

@property (nonatomic, strong) NSMutableArray *allVenues;
@property (nonatomic, strong) CLLocationManager *locationManager;

@property (strong, nonatomic) IBOutlet UIImageView *pizzaOrSadFaceImage;
@property (weak, nonatomic) IBOutlet UILabel *closestOrNoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *moreResultsLabel;
@property (weak, nonatomic) IBOutlet UILabel *venuesInformationLabel;
@property (weak, nonatomic) IBOutlet UIButton *moreResultsButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *restartButton;



- (IBAction)resetLocation:(id)sender;

@end
