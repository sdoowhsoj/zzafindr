//
//  ZZAStartUpViewController.m
//  ZZAFindr
//
//  Created by Josh Woods on 4/25/14.
//  Copyright (c) 2014 sdoowhsoj. All rights reserved.
//

#import "ZZAStartUpViewController.h"

@interface ZZAStartUpViewController ()

@end

@implementation ZZAStartUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)goToNextView {
    [self performSegueWithIdentifier:@"initialSegue" sender:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0.945 green:0.851 blue:0.6 alpha:1];
    [self performSelector:@selector(goToNextView) withObject:nil afterDelay:3];
}

- (void)viewDidAppear:(BOOL)animated
{
    CATransition* transition = [CATransition animation];
    transition.startProgress = 0;
    transition.endProgress = 3.0;
    transition.type = @"flip";
    transition.subtype = @"fromLeft";
    transition.duration = 2;
    transition.repeatCount = 1;
    [self.spinningPIcture.layer addAnimation:transition forKey:@"transition"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
