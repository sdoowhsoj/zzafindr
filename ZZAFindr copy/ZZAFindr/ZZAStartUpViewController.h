//
//  ZZAStartUpViewController.h
//  ZZAFindr
//
//  Created by Josh Woods on 4/25/14.
//  Copyright (c) 2014 sdoowhsoj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZZAStartUpViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *spinningPIcture;

@end
