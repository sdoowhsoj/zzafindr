//
//  ZZATenMilesViewController.m
//  ZZAFindr
//
//  Created by Josh Woods on 4/24/14.
//  Copyright (c) 2014 sdoowhsoj. All rights reserved.
//

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#import "ZZATenMilesViewController.h"
#import "ZZAVenue.h"
#define ywsid @"uhdAgMc2ViejHvGQixqfuQ"
#import "ZZATableViewController.h"


@interface ZZATenMilesViewController ()

@end

@implementation ZZATenMilesViewController
{
    CABasicAnimation *theAnimation;
}

- (IBAction)resetLocation:(id)sender
{
    [self.allVenues removeAllObjects];
    NSLog(@"%lu", (unsigned long)self.allVenues.count);
    [self setLabelsToBlank];
    [self performSegueWithIdentifier:@"restartSegue" sender:self];
    [self.locationManager startUpdatingLocation];
}

-(void)theAnimation
{
    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration=1.0;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:0.0];
}

-(void)setLabelsToBlank
{
    self.closestOrNoneLabel.hidden = YES;
    self.moreResultsLabel.hidden = YES;
    self.venuesInformationLabel.text = @"";
    self.moreResultsButton.hidden = YES;
    self.restartButton.enabled = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0.945 green:0.851 blue:0.6 alpha:1];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.749 green:0.224 blue:0.173 alpha:1];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor colorWithRed:0.945 green:0.851 blue:0.6 alpha:1], NSForegroundColorAttributeName,
                                                                     [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:24.0], NSFontAttributeName, nil]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"TEN MILES VIEW STARTED");
    [self setLabelsToBlank];
    [self theAnimation];
    
    //verbiage logic
    if(self.allVenues.count == 2)
    {
        self.restartButton.enabled = YES;
        self.closestOrNoneLabel.hidden = NO;
        ZZAVenue *closest = self.allVenues[0];
        self.venuesInformationLabel.text = [NSString stringWithFormat:@"%@", closest.name];
        self.moreResultsLabel.hidden = NO;
        self.moreResultsLabel.text = [NSString stringWithFormat:@"There is 1 more restaurant near you!"];
        self.moreResultsButton.hidden = NO;
        UIImage *pizza = [UIImage imageNamed:@"pizza"];
        [self.pizzaOrSadFaceImage setImage:pizza];
        [self.moreResultsButton.layer addAnimation:theAnimation forKey:@"animateOpacity"];
    } else if(self.allVenues.count >= 3)
    {
        self.restartButton.enabled = YES;
        self.closestOrNoneLabel.hidden = NO;
        ZZAVenue *closest = self.allVenues[0];
        self.venuesInformationLabel.text = [NSString stringWithFormat:@"%@", closest.name];
        self.moreResultsLabel.hidden = NO;
        self.moreResultsLabel.text = [NSString stringWithFormat:@"There are %lu more restaurants near you!", (long)(self.allVenues.count - 1)];
        self.moreResultsButton.hidden = NO;
        UIImage *pizza = [UIImage imageNamed:@"pizza"];
        [self.pizzaOrSadFaceImage setImage:pizza];
        [self.moreResultsButton.layer addAnimation:theAnimation forKey:@"animateOpacity"];
    } else if (self.allVenues.count == 1){
        self.restartButton.enabled = YES;
        self.closestOrNoneLabel.hidden = NO;
        ZZAVenue *closest = self.allVenues[0];
        self.venuesInformationLabel.text = [NSString stringWithFormat:@"%@", closest.name];
        self.moreResultsLabel.hidden = NO;
        self.moreResultsLabel.text = @"There is only 1 restaurant near you!";
        self.moreResultsButton.hidden = NO;
        UIImage *pizza = [UIImage imageNamed:@"pizza"];
        [self.pizzaOrSadFaceImage setImage:pizza];
        [self.moreResultsButton.layer addAnimation:theAnimation forKey:@"animateOpacity"];
    } else {
        self.restartButton.enabled = YES;
        self.closestOrNoneLabel.hidden = NO;
        self.closestOrNoneLabel.text = @"There is no pizza near you!";
        self.venuesInformationLabel.text = @"Click refresh above to try again!";
        self.moreResultsLabel.hidden = YES;
        self.moreResultsButton.hidden = YES;
        UIImage *sadFace = [UIImage imageNamed:@"sad"];
        [self.pizzaOrSadFaceImage setImage:sadFace];
    }
    
    NSLog(@"%lu", (unsigned long)self.allVenues.count);
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender
{
    ZZATableViewController *transferViewController = segue.destinationViewController;
    
    NSLog(@"prepareForSegue: %@", segue.identifier);
    if([segue.identifier isEqualToString:@"tenMileTableViewSegue"])
    {
        transferViewController.allVenues = _allVenues;
        NSLog(@"%lu", (unsigned long)[self.allVenues count]);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
